import {Fragment} from 'react'
import {Container} from 'react-bootstrap'
import AppNavbar from './components/AppNavbar'
// import Banner from './components/Banner'
// import Highlights from './components/Highlights'
import Courses from './pages/Courses'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import './App.css';

function App() {
  return (
  <Fragment>
    <AppNavbar/>
    <Container>   
    {/*  <Banner/>
      <Register/>
      <Highlights/>*/}
      <Login/>
      <Home/>
    <Courses/>
    </Container>
  </Fragment>
  );
}

export default App;
